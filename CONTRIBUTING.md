Contributing to Mobicoop !
=======

![Logo mobicoop](logo.jpg)

## How to contribute to Mobicoop 

*Don't forget to install all dependencies before (check README.md)*

- Go into project's root folder
- Run your console
- execute `npm run contribute`
- Follow all steps and fill in asked information.

### 🎉Welcome to Mobicoop's contributing program !🎉